// Configure the Google Cloud provider
provider "google" {
 credentials = "${file("projet-final.json")}"
 project     = "projet2-241721"
 region      = "europe-west1"
}


// Terraform plugin for creating random ids
resource "random_id" "instance_id" {
 byte_length = 8
}

// A single Google Cloud Engine instance
resource "google_compute_instance" "default" {
 name         = "vm1"
 machine_type = "n1-standard-2"
 zone         = "europe-west1-b"

 boot_disk {
   initialize_params {
     image = "ubuntu-minimal-1604-xenial-v20190514"
     size = "30"
    type = "pd-standard"
 }
}
metadata = {
   sshKeys = "pape:${file("C:/terraform/ssh/public")}"
}

 
// Make sure flask is installed on all new instances for later steps
 metadata_startup_script = "sudo apt-get update; sudo apt-get install -yq build-essential python-pip rsync; pip install flask"
 
  network_interface {
  network = "default"

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
  }
  resource "google_compute_firewall" "default" {
 name    = "my-firewall"
 network = "default"

 allow {
   protocol = "tcp"
   ports    = ["80", "443"]
 }
}

